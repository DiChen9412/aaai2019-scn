import tensorflow as tf
FLAGS = tf.app.flags.FLAGS
tf.app.flags.DEFINE_integer('on_aida', 1, 'is this running on aida')
tf.app.flags.DEFINE_string('model_dir', './model/','path to store model')
tf.app.flags.DEFINE_string('summary_dir', './summary','path to store summary_dir')
tf.app.flags.DEFINE_string('config_dir', './config.py','path to config.py')
tf.app.flags.DEFINE_string('checkpoint_path', './model/model-617','The path to a checkpoint from which to fine-tune.')
tf.app.flags.DEFINE_string('visual_dir', 'visualization/','path to store visualization codes and data')

tf.app.flags.DEFINE_string('data_dir', '../data/loc_100sp_nlcd2006_other_img_BCR1314NY_m5_m5.npy','The path of input observation data')
tf.app.flags.DEFINE_string('image_dir', '../data/BCR1314NY_m5_m5_image256.npy','The path of input image data')
tf.app.flags.DEFINE_string('srd_dir', '../data/loc_ny_nlcd2006.npy','The path of input srd data')
tf.app.flags.DEFINE_string('train_idx', '../data/train_uniform_img_idx.npy','The path of training data index')
tf.app.flags.DEFINE_string('valid_idx', '../data/valid_uniform_img_idx.npy','The path of validation data index')
tf.app.flags.DEFINE_string('test_idx', '../data/test_uniform_img_idx.npy','The path of testing data index')
tf.app.flags.DEFINE_string('weights_dir', '../data/data_weights.npy','The path of input observation data')

tf.app.flags.DEFINE_integer('batch_size', 128, 'number of data points in one batch') #128
tf.app.flags.DEFINE_integer('testing_size', 128, 'the maximal number of data points in testing or validation batch') #128
tf.app.flags.DEFINE_float('learning_rate', 0.0001, 'initial learning rate')
tf.app.flags.DEFINE_float('pred_lr', 1, 'the learning rate for predictor')
tf.app.flags.DEFINE_integer('max_epoch', 200, 'max epoch to train')
tf.app.flags.DEFINE_integer('pretrain_epoch', 100, '#epoch to pre-train')
tf.app.flags.DEFINE_float('weight_decay', 0.00001, 'weight_decay')
tf.app.flags.DEFINE_float('threshold', 0, 'The threshold for prediction')
tf.app.flags.DEFINE_float('lr_decay_ratio', 0.5, 'The decay ratio of learning rate')
tf.app.flags.DEFINE_float('lambda1', 0, 'The sampling times for training') #100
tf.app.flags.DEFINE_float('lambda2', 0.1, 'The sampling times for training') 
tf.app.flags.DEFINE_float('beta', 0.9, 'The sampling times for training') 

tf.app.flags.DEFINE_float('lr_decay_times', 5, 'How many times does learning rate decay')
tf.app.flags.DEFINE_integer('n_test_sample', 10000, 'The sampling times for testing')
tf.app.flags.DEFINE_integer('n_train_sample', 100, 'The sampling times for training') #100



tf.app.flags.DEFINE_integer('z_dim', 25, 'z dimention')

tf.app.flags.DEFINE_integer('r_offset', 0, 'the offset of labels')
tf.app.flags.DEFINE_integer('r_dim', 50, 'r/reallabel dimention')
tf.app.flags.DEFINE_integer('r_max_dim', 100, 'r_max/max label dimention')
tf.app.flags.DEFINE_integer('Gdim', 384, 'dim of feature space')

tf.app.flags.DEFINE_integer('loc_offset', 2, 'the offset between features and labels') #100
tf.app.flags.DEFINE_integer('nlcd_dim', 16, 'nlcd dimention')
tf.app.flags.DEFINE_integer('user_dim', 0, 'user-feature dimention')

tf.app.flags.DEFINE_integer('image_dim0', 256, 'image dimention-0')
tf.app.flags.DEFINE_integer('image_dim1', 256, 'image dimention-1')
tf.app.flags.DEFINE_integer('image_dim2', 3, 'image dimention-2')

tf.app.flags.DEFINE_float('save_epoch', 1.0, 'epochs to save model')
tf.app.flags.DEFINE_integer('max_keep', 3, 'maximum number of saved model')
tf.app.flags.DEFINE_integer('check_freq', 10, 'checking frequency')



