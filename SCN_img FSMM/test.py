import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import model
import get_data 
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score
#import matplotlib.pyplot as plt
import math
import urllib
#from pyheatmap.heatmap import HeatMap
#import seaborn as sns
import os 
import sys 
if (os.path.dirname(sys.argv[0])!=''):
	os.chdir(os.path.dirname(sys.argv[0]))

os.environ['CUDA_VISIBLE_DEVICES']='0'

FLAGS = tf.app.flags.FLAGS
def plot_heatmap(srd, sess, classifier, avg_user):

    real_batch_size=min(FLAGS.testing_size, len(srd))

    data = []
    meanX = np.load("../data/meanX.npy")[:FLAGS.nlcd_dim]
    stdX = np.load("../data/stdX.npy")[:FLAGS.nlcd_dim]


    for i in range(int( (len(srd)-1)/real_batch_size )+1):

		start = real_batch_size*i
		end = min(real_batch_size*(i+1), len(srd))

		loc = get_data.get_loc(srd, np.arange(start,end))
		input_nlcd = get_data.get_nlcd(srd, np.arange(start,end), FLAGS.loc_offset)

		if ("norm" in FLAGS.data_dir):
			input_nlcd = (input_nlcd - meanX)/(stdX+1e-6)

		#print "nlcd", np.mean(input_nlcd, axis=0), np.std(input_nlcd, axis=0)
		#break
		input_user = np.tile(avg_user, (end - start, 1))
		feed_dict={}
		feed_dict[classifier.input_nlcd]=input_nlcd #np.concatenate((input_nlcd,input_user),1)
		feed_dict[classifier.keep_prob]=1.0
		indiv_prob = sess.run([classifier.indiv_prob],feed_dict)[0]
		#print indiv_prob.shape
		data+=list(np.concatenate((loc, indiv_prob), axis = 1))


    minlat=minlont=1e10
    maxlat=maxlont=-1e10
    lenX=100
    lenY=100
    for L in data:
        minlat=min(minlat,L[0])
        maxlat=max(maxlat,L[0])
        minlont=min(minlont,L[1])
        maxlont=max(maxlont,L[1])

    print "range latitude", minlat, maxlat
    print "range longitude", minlont, maxlont
    point=np.zeros((lenX+1, lenY+1))
    count=np.zeros((lenX+1, lenY+1))+1e-6
    eps=1e-6
    for L in data:
        lat=L[0]
        lont=L[1]
        x=int(math.floor(lenX*(lat-minlat)/(maxlat-minlat)))
        y=int(math.floor(lenY*(lont-minlont)/(maxlont-minlont)))

        p=L[2]
        point[lenX-x][y]+=p
        count[lenX-x][y]+=1

    point /= count

    f, ax = plt.subplots()
    ax = sns.heatmap(point, vmin =0, vmax = 1, cmap="jet")
    #plt.savefig("heatmap");
    plt.show()

def analysis(species, indiv_prob, input_label, printNow = False):
	

	TP = 0
	TN = 0
	FN = 0
	FP = 0
	pred_label = np.greater(indiv_prob, 0.5).astype(int)

	#print input_label.shape 
	for j in range(input_label.shape[1]):
		for i in range(input_label.shape[0]):
			if (pred_label[i][j]==1 and input_label[i][j]==1):
				TP+=1
			if (pred_label[i][j]==1 and input_label[i][j]==0):
				FP+=1
			if (pred_label[i][j]==0 and input_label[i][j]==0):
				TN+=1
			if (pred_label[i][j]==0 and input_label[i][j]==1):
				FN+=1

	N = (TP+TN+FN+FP)*1.0
	eps = 1e-6
	precision = 1.0 * TP / (TP + FP + eps)
	recall = 1.0 * TP / (TP + FN + eps) 
	Accuracy =  1.0*(TN+TP)/(N)
	F1 = 2.0 * precision * recall / (precision + recall + eps)
	#print "F2:", (1+4)*precision*recall/(4*precision+recall)

	occurrence = np.mean(input_label)
	auc = roc_auc_score(input_label, indiv_prob)

	indiv_prob = np.reshape(indiv_prob, (-1))
	input_label = np.reshape(input_label, (-1))

	new_auc = roc_auc_score(input_label, indiv_prob)

	ap = average_precision_score(input_label,indiv_prob)

	if (printNow):
		print "\nThis is the analysis of species %s:"%species
		print "occurrence rate:", occurrence
		print "Overall \tauc=%.6f\tnew_auc=%.6f\tap=%.6f" % (auc, new_auc, ap)	
		print "F1:", F1 
		print "Accuracy:", Accuracy
		print "Precision:", precision
		print "Recall:", recall
		print "TP=%f, TN=%f, FN=%f, FP=%f"%(TP/N, TN/N, FN/N, FP/N)
		print " "
		
	res = np.zeros(7)
	res[0] = auc
	res[1] = new_auc
	res[2] = ap
	res[3] = F1
	res[4] = Accuracy
	res[5] = precision
	res[6] = recall

	np.save("test_res",res)
	return occurrence, auc, F1, Accuracy, new_auc, ap,  precision, recall, TP/N, TN/N, FN/N, FP/N,

def main(_):

	print 'reading npy...'
	data = np.load(FLAGS.data_dir)
	images = np.load(FLAGS.image_dir)
	srd = np.load(FLAGS.srd_dir)
	test_idx = np.load(FLAGS.test_idx)

	offset = FLAGS.loc_offset + FLAGS.r_max_dim + 16

	avg_user = np.mean(data, axis=0)[offset:offset + 6]

	print 'reading completed'	

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)

	print 'building network...'

	classifier = model.MODEL(is_training=False)
	global_step = tf.Variable(0,name='global_step',trainable=False)

	merged_summary = tf.summary.merge_all()
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir, sess.graph)

	saver = tf.train.Saver(max_to_keep=None)
	saver.restore(sess,FLAGS.checkpoint_path)

	print 'restoring from '+FLAGS.checkpoint_path


	def test_step():
		print 'Testing...'
		all_nll_loss = 0
		all_l2_loss = 0
		all_total_loss = 0

		all_indiv_prob = []
		all_label = []

		sigma=[]
		real_batch_size=min(FLAGS.testing_size, len(test_idx))
		avg_cor=0
		cnt_cor=0
		
		for i in range(int( (len(test_idx)-1)/real_batch_size )+1):

			start = real_batch_size*i
			end = min(real_batch_size*(i+1), len(test_idx))

			input_image = get_data.get_image(images, test_idx[start:end])
			input_label = get_data.get_label(data,test_idx[start:end])
			#input_weights = np.zeros(input_nlcd.shape[0])+1

			feed_dict={}
			feed_dict[classifier.input_train_image]=input_image
			feed_dict[classifier.input_label]=input_label
			feed_dict[classifier.input_weights] = np.zeros((input_label.shape[0],1))+1
			feed_dict[classifier.keep_prob]=1.0
			feed_dict[classifier.alpha] = 0
			feed_dict[classifier.is_training] = False

			nll_loss, l2_loss, total_loss, indiv_prob= sess.run([classifier.nll_loss, classifier.l2_loss, \
				classifier.total_loss, classifier.indiv_prob],feed_dict)
			
			all_nll_loss += nll_loss*(end-start)
			all_l2_loss += l2_loss*(end-start)
			all_total_loss += total_loss*(end-start)

			if (all_indiv_prob == []):
				all_indiv_prob = indiv_prob
			else:
				all_indiv_prob = np.concatenate((all_indiv_prob, indiv_prob))

			if (all_label == []):
				all_label = input_label
			else:
				all_label = np.concatenate((all_label, input_label))

		
		#print "Overall occurrence ratio: %f"%(np.mean(all_label))
		
		nll_loss = all_nll_loss / len(test_idx)
		l2_loss = all_l2_loss / len(test_idx)
		total_loss = all_total_loss / len(test_idx)

		time_str = datetime.datetime.now().isoformat()

		print "Overall nll_loss=%.6f\tl2_loss=%.6f\ttotal_loss=%.6f \n%s" % (nll_loss, l2_loss, total_loss, time_str)	
		return all_indiv_prob, all_label


	indiv_prob, input_label = test_step()

	analysis("all", indiv_prob, input_label, True)

	summary = []
	for i in range(FLAGS.r_dim):
		sp_indiv_prob = indiv_prob[:,i].reshape(indiv_prob.shape[0],1)
		sp_input_label = input_label[:,i].reshape(input_label.shape[0],1)

		res = analysis(i, sp_indiv_prob, sp_input_label, False)
		summary.append(res)
	summary = np.asarray(summary)

	np.save("../data/summary_grid",summary)
	#plot_heatmap(srd, sess, classifier, avg_user)
	
if __name__=='__main__':
	tf.app.run()



