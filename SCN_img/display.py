import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import model
import get_data 
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import math
import urllib
from pyheatmap.heatmap import HeatMap
import seaborn as sns
FLAGS = tf.app.flags.FLAGS

def get_random_samples(idx_weights, train_idx_all):
	res = np.random.multinomial(len(idx_weights),idx_weights)
	train_idx = []

	for i in range(res.shape[0]):
		for j in range(int(res[i])):
			train_idx.append(train_idx_all[i])

	np.random.shuffle(train_idx)
	return train_idx, res


#np.random.seed(19950420)
data = np.load(FLAGS.data_dir)
data_weights = np.load(FLAGS.weights_dir)
srd = np.load(FLAGS.srd_dir)
train_idx = np.load(FLAGS.train_idx)
valid_idx = np.load(FLAGS.valid_idx)

train_idx_all = np.copy(train_idx)
sum_weights = 0
for i in train_idx:
	sum_weights += data_weights[i]

data_weights/=sum_weights
idx_weights = []
for i in train_idx:
	idx_weights.append(data_weights[i])

count_n_sampled = np.zeros(len(idx_weights))

for i in range(500):
	train_idx, res= get_random_samples(idx_weights, train_idx_all)
	count_n_sampled += res

#count_n_sampled = np.load("count_n_sampled.npy")

#idx_weights = np.load("idx_weights.npy")

#count_n_sampled /= 500.0

print idx_weights[:10], np.sum(idx_weights)

print np.mean(np.abs(np.asarray(idx_weights)*len(idx_weights)*500-count_n_sampled))