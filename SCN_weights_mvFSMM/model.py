import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS

class MODEL:

	def __init__(self,is_training):
		tf.set_random_seed(19950420)
		r_dim = FLAGS.r_dim
		
		self.input_nlcd = tf.placeholder(dtype = tf.float32,shape=[None,FLAGS.nlcd_dim],name='input_nlcd')
		
		self.input_label = tf.placeholder(dtype = tf.float32,shape=[None,FLAGS.r_dim],name='input_label')

		self.input_valid_nlcd = tf.placeholder(dtype = tf.float32, shape=[None,FLAGS.nlcd_dim],name='input_valid_nlcd')

		self.input_weights = tf.placeholder(dtype = tf.float32, shape=[None,1],name='input_weights')

		self.keep_prob = tf.placeholder(dtype = tf.float32, name = "keep_prob")

		self.alpha = tf.placeholder(dtype = tf.float32, name = "alpha")

		self.beta = tf.placeholder(dtype = tf.float32, name = "beta")

		self.bias = tf.placeholder(dtype = tf.float32, name = "bias")

		self.Ms = tf.placeholder(dtype = tf.float32, shape=[FLAGS.Gdim], name = "Ms")

		self.Mt = tf.placeholder(dtype = tf.float32, shape=[FLAGS.Gdim], name = "Mt")

		weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)

		#input_feature = tf.concat((self.input_nlcd, self.input_valid_nlcd))
		############## compute mu & sigma ###############

		self.S_1 = slim.fully_connected(self.input_nlcd, 512, weights_regularizer=weights_regularizer, scope='extractor/fc_1', reuse=False)
		self.S_2 = slim.fully_connected(self.S_1, 1024, weights_regularizer=weights_regularizer, scope='extractor/fc_2', reuse=False)
		self.S_3 = slim.fully_connected(self.S_2, FLAGS.Gdim, weights_regularizer=weights_regularizer, scope='extractor/fc_3', reuse=False)

		self.T_1 = slim.fully_connected(self.input_valid_nlcd, 512, weights_regularizer=weights_regularizer, scope='extractor/fc_1', reuse=True)
		self.T_2 = slim.fully_connected(self.T_1, 1024, weights_regularizer=weights_regularizer, scope='extractor/fc_2', reuse=True)
		self.T_3 = slim.fully_connected(self.T_2, FLAGS.Gdim, weights_regularizer=weights_regularizer, scope='extractor/fc_3', reuse=True)


		#dropout
		#feature1 = slim.dropout(self.fc_3, keep_prob=self.keep_prob, is_training=is_training)

		sourceFeature = self.S_3
		targetFeature = self.T_3			
		

		S = slim.fully_connected(sourceFeature, 512, weights_regularizer=weights_regularizer, scope='D/fc_1', reuse=False)
		S = slim.fully_connected(S, 256, weights_regularizer=weights_regularizer, scope='D/fc_2', reuse=False)

		T = slim.fully_connected(targetFeature, 512, weights_regularizer=weights_regularizer, scope='D/fc_1', reuse=True)
		T = slim.fully_connected(T, 256, weights_regularizer=weights_regularizer, scope='D/fc_2', reuse=True)

		#S = sourceFeature
		#T = targetFeature

		self.logits_source_D = slim.fully_connected(S, 1, activation_fn=None, weights_regularizer=weights_regularizer,scope='D/softmax', reuse=False) 
		self.logits_target_D = slim.fully_connected(T, 1, activation_fn=None, weights_regularizer=weights_regularizer,scope='D/softmax', reuse=True)

		eps = 1e-6

		self.indiv_prob_source_D = tf.sigmoid(self.logits_source_D)*(1 - eps) + 0.5*eps
		#tf.summary.histogram("prob_S", self.indiv_prob_source_D)

		self.indiv_prob_target_D = tf.sigmoid(self.logits_target_D)*(1 - eps) + 0.5*eps

		self.indiv_prob_D = tf.concat([self.indiv_prob_source_D, self.indiv_prob_target_D],0)


		self.nll_loss_D = -(tf.reduce_mean(tf.log(self.indiv_prob_source_D)) + tf.reduce_mean(tf.log(1 - self.indiv_prob_target_D)))*0.5




		x = slim.fully_connected(sourceFeature, 512, weights_regularizer=weights_regularizer, scope='F/fc_1')
		x = slim.fully_connected(x, 256, weights_regularizer=weights_regularizer, scope='F/fc_2', reuse=False)

		self.logits_F = slim.fully_connected(x, r_dim, activation_fn=None, weights_regularizer=weights_regularizer,scope='F/softmax')
		self.indiv_prob=tf.sigmoid(self.logits_F)*(1 - eps) + 0.5*eps
		#self.indiv_prob = tf.nn.softmax(self.logits, name='individual_prob')
		
		a = self.alpha 
		p = (self.indiv_prob_source_D - 0.5)*a + 0.5

		self.w = (1-self.indiv_prob_source_D)/ self.indiv_prob_source_D

		ms = tf.reduce_mean(sourceFeature * self.w, axis=0)
		mt = tf.reduce_mean(targetFeature, axis=0)

		self.MS = (self.Ms * self.beta + (1 - self.beta)*ms)
		self.MT = (self.Mt * self.beta + (1 - self.beta)*mt)

		MS_unbiased = self.MS / (1-self.bias)
		MT_unbiased = self.MT / (1-self.bias)

		self.KMM_loss = FLAGS.KMM_weight_decay * tf.reduce_sum(tf.square(MS_unbiased - MT_unbiased))




		self.weights = ((1 - p) / p)

		#upper = 0.9 #0.9
		#lower = 0.1 #0.1
		#Lambda = 0.5
		#T = self.input_label

		self.nll_loss_W = tf.reduce_mean(
			tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.input_label,logits=self.logits_F),1)* self.input_weights 
			)
		

		#tf.reduce_sum(
		#	tf.reduce_mean(T*(tf.square(tf.maximum(0.0,upper - self.indiv_prob))) + Lambda*(1-T)*(tf.square(tf.maximum(0.0, self.indiv_prob-lower))), axis = 1)
		#	* self.input_weights 
		#	)
		
		
		self.nll_loss= tf.reduce_mean(tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.input_label,logits=self.logits_F),1))
		

		#tf.reduce_sum(
		#	tf.reduce_mean(T*(tf.square(tf.maximum(0.0,upper - self.indiv_prob))) + 
		#		alpha*(1-T)*(tf.square(tf.maximum(0.0, self.indiv_prob-lower))), axis = 1)
		#	* self.input_weights 
		#	)/tf.reduce_sum(self.input_weights)
		#tf.reduce_mean(tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.input_label,logits=self.logits),1))
		
		
		###### loss ##############
		
		self.l2_loss = tf.add_n(tf.losses.get_regularization_losses(scope = "extractor" ) + tf.losses.get_regularization_losses(scope = "F"))
		

		self.total_loss = (self.l2_loss + self.nll_loss_W)



		self.l2_loss_D = tf.add_n(tf.losses.get_regularization_losses(scope = "extractor" ) + tf.losses.get_regularization_losses(scope = "D"))
		
		self.total_loss_D = (self.l2_loss_D + self.nll_loss_D + self.KMM_loss)


		if (FLAGS.on_aida == 0):
			tf.summary.histogram("weights", self.weights)
			tf.summary.histogram("p", p)
			tf.summary.histogram("raw_p", self.indiv_prob_source_D)
			tf.summary.scalar('l2_loss',self.l2_loss)
			tf.summary.scalar("alpha", self.alpha)
			tf.summary.scalar('nll_loss',self.nll_loss)
			tf.summary.scalar('nll_loss_D',self.nll_loss_D)
			tf.summary.scalar('nll_loss_W',self.nll_loss_W)
			tf.summary.scalar('KMM_loss',self.KMM_loss)
			tf.summary.scalar('total_loss',self.total_loss)


