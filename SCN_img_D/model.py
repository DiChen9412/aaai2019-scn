import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS
def bn_act_conv_drp(current, num_outputs, kernel_size, scope='block'): 
    current = slim.batch_norm(current, scope=scope + '_bn')  
    current = tf.nn.relu(current)  
    current = slim.conv2d(current, num_outputs, kernel_size, scope=scope + '_conv')  
    current = slim.dropout(current, scope=scope + '_dropout')  
    return current  

def block(net, layers, growth, scope='block'):  
    """ 
    Args: 
        layers: #layers of dense block
        growth: k
    """  
    for idx in range(layers):  
        """ 
        BN-ReLU-Conv(1x1)-DroupOut-BN-ReLU-Conv(3x3)-DroupOut 
        """  
        bottleneck = bn_act_conv_drp(net, 4 * growth, [1, 1], scope=scope + '_conv1x1-' + str(idx))  
        tmp = bn_act_conv_drp(bottleneck, growth, [3, 3], scope=scope + '_conv3x3-' + str(idx))  
        net = tf.concat(axis=3, values=[net, tmp])  
    return net 

def transition(net, num_outputs, scope='transition'):  
    net = bn_act_conv_drp(net, num_outputs, [1, 1], scope=scope + '_conv1x1')  
    net = slim.avg_pool2d(net, [2, 2], stride=2, scope=scope + '_avgpool')  
    return net  

def densenet(images, is_training=False,  dropout_keep_prob=0.8, scope='DenseNet', is_reuse = False):  
    """Creates a variant of the densenet model. 
 
      images: A batch of `Tensors` of size [batch_size, height, width, channels]. 
      num_classes: the number of classes in the dataset. 
      is_training: specifies whether or not we're currently training the model. 
        This variable will determine the behaviour of the dropout layer. 
      dropout_keep_prob: the percentage of activation values that are retained. 
      prediction_fn: a function to get predictions out of logits. 
      scope: Optional variable_scope. 
 
    Returns: 
      logits: the pre-softmax activations, a tensor of size 
        [batch_size, `num_classes`] 

      end_points: a dictionary from components of the network to the corresponding 
        activation. 
    """  
    growth = 12  
    compression_rate = 0.5  
  
    def reduce_dim(input_feature):  
        return int(int(input_feature.shape[-1]) * compression_rate)  
  
    end_points = {}  
    if (is_reuse == False):
        is_reuse = None

    with tf.variable_scope(scope, 'DenseNet', [images], reuse = is_reuse):  
        with slim.arg_scope([slim.batch_norm], is_training=is_training, decay=0.97, epsilon=1e-5):
            with slim.arg_scope([slim.dropout], is_training=is_training, keep_prob=dropout_keep_prob):
                with slim.arg_scope([slim.conv2d], weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)):
                    net = images  
                    print "layer1",net.shape
                    net = slim.conv2d(net, 2*growth, 7, stride=2, scope='conv1')  
                    print "layer2",net.shape
                    net = slim.max_pool2d(net, [3,3], stride=2, padding='SAME', scope='pool1')  
                    print "layer3",net.shape

                    blocks_list=[6, 12, 24, 16]
                    for i in range(len(blocks_list)-1):
                        net = block(net, blocks_list[i], growth, scope='block%d'%i)  
                        print "layer%d"%(2*i+4), net.shape

                        net = transition(net, reduce_dim(net), scope='transition%d'%i)  
                        print "layer%d"%(2*i+5), net.shape
          
                    net = block(net, blocks_list[-1], growth, scope='block%d'%(len(blocks_list)-1))  
                    print "layer10", net.shape
                    net = slim.batch_norm(net, scope='last_batch_norm_relu')  
                    print "layer11", net.shape

                    net = tf.nn.relu(net)  
          
                    # Global average pooling.  
                    print "before global", net.shape

                    
                    net = tf.reduce_mean(net, [1, 2], name='pool2', keep_dims=True)  
                    net = tf.squeeze(net, [1, 2], name='SpatialSqueeze')  
                    print "final output", net.shape

                    """
                    biases_initializer = tf.constant_initializer(0.1)  
                    net = slim.conv2d(net, num_classes, [1, 1], biases_initializer=biases_initializer, scope='logits')  
                    #print "after global2", net.shape
                    print "after global", net.shape
                    logits = tf.squeeze(net, [1, 2], name='SpatialSqueeze')  

                    print "logits", logits.shape
                    predictions = slim.softmax(logits, scope='predictions') 
                    print "predictions", predictions.shape
                    #end_points['logits'] = logits  
                    #end_points['predictions'] = predictions
                    #print "predictions", end_points['predictions'].shape
                    """
    return net #logits, predictions  


class MODEL:

    def __init__(self,is_training):
        tf.set_random_seed(19950420)
        r_dim = FLAGS.r_dim
        
        self.input_train_image = tf.placeholder(dtype = tf.float32,shape=[None, FLAGS.image_dim0, FLAGS.image_dim1, FLAGS.image_dim2], name='input_train_image')

        self.input_valid_image = tf.placeholder(dtype = tf.float32, shape=[None, FLAGS.image_dim0, FLAGS.image_dim1, FLAGS.image_dim2],name='input_valid_image')
        
        self.input_label = tf.placeholder(dtype = tf.float32,shape=[None,FLAGS.r_dim],name='input_label')

        self.input_weights = tf.placeholder(dtype = tf.float32, shape=[None,1],name='input_weights')

        self.keep_prob = tf.placeholder(dtype = tf.float32, name = "keep_prob")

        self.alpha = tf.placeholder(dtype = tf.float32, name = "alpha")

        self.is_training = tf.placeholder(tf.bool) 

        self.beta = tf.placeholder(dtype = tf.float32, name = "beta")

        self.bias = tf.placeholder(dtype = tf.float32, name = "bias")

        self.Ms = tf.placeholder(dtype = tf.float32, shape=[FLAGS.Gdim], name = "Ms")

        self.Mt = tf.placeholder(dtype = tf.float32, shape=[FLAGS.Gdim], name = "Mt")



        weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)

        sourceFeature = densenet(self.input_train_image, is_training=self.is_training,  dropout_keep_prob=self.keep_prob,\
         scope='extractor/DenseNet', is_reuse = False)
        targetFeature = densenet(self.input_valid_image, is_training=self.is_training,  dropout_keep_prob=self.keep_prob,\
         scope='extractor/DenseNet', is_reuse = True)   
        

        S = slim.fully_connected(sourceFeature, 512, weights_regularizer=weights_regularizer, scope='D/fc_1', reuse=False)
        S = slim.fully_connected(S, 256, weights_regularizer=weights_regularizer, scope='D/fc_2', reuse=False)

        T = slim.fully_connected(targetFeature, 512, weights_regularizer=weights_regularizer, scope='D/fc_1', reuse=True)
        T = slim.fully_connected(T, 256, weights_regularizer=weights_regularizer, scope='D/fc_2', reuse=True)

        #S = sourceFeature
        #T = targetFeature

        self.logits_source_D = slim.fully_connected(S, 1, activation_fn=None, weights_regularizer=weights_regularizer,scope='D/softmax', reuse=False) 
        self.logits_target_D = slim.fully_connected(T, 1, activation_fn=None, weights_regularizer=weights_regularizer,scope='D/softmax', reuse=True)

        eps = 1e-6

        self.indiv_prob_source_D = tf.sigmoid(self.logits_source_D)*(1 - eps) + 0.5*eps
        #tf.summary.histogram("prob_S", self.indiv_prob_source_D)

        self.indiv_prob_target_D = tf.sigmoid(self.logits_target_D)*(1 - eps) + 0.5*eps

        self.indiv_prob_D = tf.concat([self.indiv_prob_source_D, self.indiv_prob_target_D],0)

        self.nll_loss_D = -(tf.reduce_mean(tf.log(self.indiv_prob_source_D)) + tf.reduce_mean(tf.log(1 - self.indiv_prob_target_D)))*0.5




        x = slim.fully_connected(sourceFeature, 512, weights_regularizer=weights_regularizer, scope='F/fc_1', reuse = False)
        x = slim.fully_connected(x, 256, weights_regularizer=weights_regularizer, scope='F/fc_2', reuse=False)

        self.logits_F = slim.fully_connected(x, r_dim, activation_fn=None, weights_regularizer=weights_regularizer,scope='F/softmax')
        self.indiv_prob=tf.sigmoid(self.logits_F)*(1 - eps) + 0.5*eps
        #self.indiv_prob = tf.nn.softmax(self.logits, name='individual_prob')
        
        a = self.alpha 
        p = (self.indiv_prob_source_D - 0.5)*a + 0.5

        self.w = (1-self.indiv_prob_source_D)/ self.indiv_prob_source_D

        ms = tf.reduce_mean(sourceFeature * self.w, axis=0)
        mt = tf.reduce_mean(targetFeature, axis=0)

        self.MS = (self.Ms * self.beta + (1 - self.beta)*ms)
        self.MT = (self.Mt * self.beta + (1 - self.beta)*mt)

        MS_unbiased = self.MS / (1-self.bias)
        MT_unbiased = self.MT / (1-self.bias)

        self.KMM_loss = tf.reduce_sum(tf.square(MS_unbiased - MT_unbiased))

        #self.KMM_loss = tf.reduce_sum(tf.square(tf.reduce_mean(sourceFeature * self.w, axis=0) - tf.reduce_mean(targetFeature, axis=0)))


        self.weights = ((1 - p) / p)

        #tf.summary.histogram("weights", self.weights)
        #tf.summary.histogram("p", p)
        #tf.summary.histogram("raw_p", self.indiv_prob_source_D)
        #upper = 0.9 #0.9
        #lower = 0.1 #0.1
        #Lambda = 0.5
        #T = self.input_label

        self.nll_loss_W = tf.reduce_mean(
            tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.input_label,logits=self.logits_F),1)* self.input_weights 
            )
        

        #tf.reduce_sum(
        #   tf.reduce_mean(T*(tf.square(tf.maximum(0.0,upper - self.indiv_prob))) + Lambda*(1-T)*(tf.square(tf.maximum(0.0, self.indiv_prob-lower))), axis = 1)
        #   * self.input_weights 
        #   )
        
        
        self.nll_loss= tf.reduce_mean(tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.input_label,logits=self.logits_F),1))
        

        #tf.reduce_sum(
        #   tf.reduce_mean(T*(tf.square(tf.maximum(0.0,upper - self.indiv_prob))) + 
        #       alpha*(1-T)*(tf.square(tf.maximum(0.0, self.indiv_prob-lower))), axis = 1)
        #   * self.input_weights 
        #   )/tf.reduce_sum(self.input_weights)
        #tf.reduce_mean(tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.input_label,logits=self.logits),1))
        
        
        self.l2_loss = tf.add_n(tf.losses.get_regularization_losses(scope = "extractor" ) + tf.losses.get_regularization_losses(scope = "F"))
        

        self.total_loss = (self.l2_loss + self.nll_loss_W)



        self.l2_loss_D = tf.add_n(tf.losses.get_regularization_losses(scope = "extractor" ) + tf.losses.get_regularization_losses(scope = "D"))
        
        self.total_loss_D = (self.l2_loss_D + FLAGS.lambda1*self.nll_loss_D + FLAGS.lambda2*self.KMM_loss)


        if (FLAGS.on_aida == 0):
            tf.summary.histogram("weights", self.weights)
            tf.summary.histogram("p", p)
            tf.summary.histogram("raw_p", self.indiv_prob_source_D)
            tf.summary.scalar('l2_loss',self.l2_loss)
            tf.summary.scalar("alpha", self.alpha)
            tf.summary.scalar('nll_loss',self.nll_loss)
            tf.summary.scalar('nll_loss_D',self.nll_loss_D)
            tf.summary.scalar('nll_loss_W',self.nll_loss_W)
            tf.summary.scalar('KMM_loss',self.KMM_loss)
            tf.summary.scalar('total_loss',self.total_loss)


