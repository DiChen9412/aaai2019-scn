import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS

class MODEL:

	def __init__(self,is_training):
		tf.set_random_seed(19950420)
		r_dim = FLAGS.r_dim
		self.input_nlcd = tf.placeholder(dtype=tf.float32,shape=[None, FLAGS.nlcd_dim],name='input_nlcd')
		self.input_valid_nlcd = tf.placeholder(dtype=tf.float32,shape=[None,FLAGS.nlcd_dim],name='input_valid_nlcd')

		self.keep_prob = tf.placeholder(tf.float32)
		weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)

		############## compute mu & sigma ###############
		self.S_1 = slim.fully_connected(self.input_nlcd, 512, weights_regularizer=weights_regularizer, scope='generator/fc_1', reuse=False)
		self.S_2 = slim.fully_connected(self.S_1, 1024, weights_regularizer=weights_regularizer, scope='generator/fc_2', reuse=False)
		self.S_3 = slim.fully_connected(self.S_2, 512, weights_regularizer=weights_regularizer, scope='generator/fc_3', reuse=False)

		

		self.T_1 = slim.fully_connected(self.input_valid_nlcd, 512, weights_regularizer=weights_regularizer, scope='generator/fc_1', reuse=True)
		self.T_2 = slim.fully_connected(self.T_1, 1024, weights_regularizer=weights_regularizer, scope='generator/fc_2', reuse=True)
		self.T_3 = slim.fully_connected(self.T_2, 512, weights_regularizer=weights_regularizer, scope='generator/fc_3', reuse=True)



		eps = 1e-6
		
		self.logits_S = slim.fully_connected(self.S_3, 1, activation_fn=None, weights_regularizer=weights_regularizer,scope='generator/logits', reuse=False)
		self.indiv_prob_S=tf.sigmoid(self.logits_S)*(1 - eps) + 0.5*eps

		self.logits_T = slim.fully_connected(self.T_3, 1, activation_fn=None, weights_regularizer=weights_regularizer,scope='generator/logits', reuse=True)
		self.indiv_prob_T=tf.sigmoid(self.logits_T)*(1 - eps) + 0.5*eps



		self.nll_loss= ( tf.reduce_mean(tf.reduce_sum(-tf.log(self.indiv_prob_S),1)) + tf.reduce_mean(tf.reduce_sum(-tf.log(1 - self.indiv_prob_T),1))) *0.5
		
		self.indiv_prob = tf.concat((self.indiv_prob_S,self.indiv_prob_T),0)
		###### loss ##############
		tf.summary.scalar('nll_loss',self.nll_loss)

		self.l2_loss = tf.add_n(tf.losses.get_regularization_losses())
		tf.summary.scalar('l2_loss',self.l2_loss)
		
		self.total_loss = self.l2_loss + self.nll_loss
		tf.summary.scalar('total_loss',self.total_loss)

		
