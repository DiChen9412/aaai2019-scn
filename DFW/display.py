# -*- coding:UTF-8 -*-
from pyheatmap.heatmap import HeatMap
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import math
import plotly.plotly as py
import plotly.graph_objs as go



data_weights=np.load("learned_weights.npy")
print data_weights.shape

w = np.ones_like(data_weights)/float(len(data_weights))

n, bines, patches = plt.hist(data_weights, 100, weights=w, alpha=0.75)

plt.show()
