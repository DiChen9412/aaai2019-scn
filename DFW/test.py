import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import model
import get_data 
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score
import matplotlib.pyplot as plt
import math
import urllib
from pyheatmap.heatmap import HeatMap
import seaborn as sns

FLAGS = tf.app.flags.FLAGS
def plot_heatmap(sess, classifier):

    srd = np.load(FLAGS.srd_dir)

    real_batch_size=min(FLAGS.testing_size, len(srd))

    data = []

    for i in range(int( (len(srd)-1)/real_batch_size )+1):

		start = real_batch_size*i
		end = min(real_batch_size*(i+1), len(srd))

		loc = get_data.get_loc(srd, np.arange(start,end))
		input_nlcd = get_data.get_nlcd(srd, np.arange(start,end), FLAGS.loc_offset)

		feed_dict={}
		feed_dict[classifier.input_nlcd]=input_nlcd
		feed_dict[classifier.keep_prob]=1.0

		indiv_prob = sess.run([classifier.indiv_prob_S],feed_dict)[0]

		p = (1-indiv_prob)/indiv_prob
		#print indiv_prob.shape
		data+=list(np.concatenate((loc, indiv_prob), axis = 1))


    minlat=minlont=1e10
    maxlat=maxlont=-1e10
    lenX=100
    lenY=100
    for L in data:
        minlat=min(minlat,L[0])
        maxlat=max(maxlat,L[0])
        minlont=min(minlont,L[1])
        maxlont=max(maxlont,L[1])

    print "range latitude", minlat, maxlat
    print "range longitude", minlont, maxlont
    point=np.zeros((lenX+1, lenY+1))
    count=np.zeros((lenX+1, lenY+1))+1e-6
    eps=1e-6
    for L in data:
        lat=L[0]
        lont=L[1]
        x=int(math.floor(lenX*(lat-minlat)/(maxlat-minlat)))
        y=int(math.floor(lenY*(lont-minlont)/(maxlont-minlont)))

        p=L[2]
        point[lenX-x][y]+=p
        count[lenX-x][y]+=1

    point /= count

    f, ax = plt.subplots()
    ax = sns.heatmap(point, cmap="jet")
    #plt.savefig("heatmap");
    plt.show()


def main(_):

	print 'reading npy...'

	data = np.load(FLAGS.data_dir)
	train_idx = np.load(FLAGS.train_idx)

	print 'reading completed'

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)

	print 'building network...'

	classifier = model.MODEL(is_training=False)
	global_step = tf.Variable(0,name='global_step',trainable=False)

	merged_summary = tf.summary.merge_all()
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir, sess.graph)

	saver = tf.train.Saver(max_to_keep=None)
	saver.restore(sess,FLAGS.checkpoint_path)

	print 'restoring from '+FLAGS.checkpoint_path


	def test_step():
		print 'Testing...'
		all_nll_loss = 0
		all_l2_loss = 0
		all_total_loss = 0

		all_indiv_prob = []
		all_label = []

		sigma=[]
		real_batch_size=min(FLAGS.batch_size, len(train_idx))
		avg_cor=0
		cnt_cor=0
		
		for i in range(int( (len(train_idx)-1)/real_batch_size )+1):

			start = real_batch_size*i
			end = min(real_batch_size*(i+1), len(train_idx))

			#input_image= get_data.get_image(images, test_idx[start:end])
			input_nlcd = get_data.get_nlcd(data,train_idx[start:end])

			feed_dict={}
			feed_dict[classifier.input_nlcd]=input_nlcd
			#feed_dict[classifier.input_image]=input_image
			feed_dict[classifier.keep_prob]=1.0
			

			indiv_prob= sess.run([classifier.indiv_prob_S],feed_dict)[0]
			

			if (all_indiv_prob == []):
				all_indiv_prob = indiv_prob
			else:
				all_indiv_prob = np.concatenate((all_indiv_prob, indiv_prob))
		
		#print "Overall occurrence ratio: %f"%(np.mean(all_label))
		
		return all_indiv_prob


	indiv_prob = test_step()
	weights = np.zeros(data.shape[0])
	for i in range(train_idx.shape[0]):
		j = train_idx[i]
		prob = indiv_prob[i]
		prob = prob * (1- 1e-4) + 1e-4
		weights[j] = (1.0 - prob)/prob
		
	np.save("learned_weights", weights)
	plot_heatmap(sess, classifier)
	
if __name__=='__main__':
	tf.app.run()



