import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import model
import get_data
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

FLAGS = tf.app.flags.FLAGS

def get_random_samples(idx_weights, train_idx_all):
	res = np.random.multinomial(len(idx_weights),idx_weights)
	train_idx = []

	for i in range(res.shape[0]):
		for j in range(int(res[i])):
			train_idx.append(train_idx_all[i])

	np.random.shuffle(train_idx)
	return train_idx, res

	
def MakeSummary(name, value):
	"""Creates a tf.Summary proto with the given name and value."""
	summary = tf.Summary()
	val = summary.value.add()
	val.tag = str(name)
	val.simple_value = float(value)
	return summary

#def train_step(input_nlcd, input_label, smooth_ce_loss, smooth_l2_loss,smooth_total_loss):
def train_step(sess, hg, merged_summary, summary_writer, input_nlcd, input_valid_nlcd, train_op, global_step):

	feed_dict={}

	feed_dict[hg.input_nlcd] = input_nlcd
	feed_dict[hg.input_valid_nlcd] = input_valid_nlcd
	feed_dict[hg.keep_prob] = 0.8

	temp, step, nll_loss, l2_loss, total_loss, summary, indiv_prob= \
	sess.run([train_op, global_step, hg.nll_loss, hg.l2_loss, hg.total_loss,\
	 merged_summary, hg.indiv_prob], feed_dict)

	time_str = datetime.datetime.now().isoformat()
	summary_writer.add_summary(summary,step)

	return indiv_prob, nll_loss, l2_loss, total_loss

def main(_):

	print 'reading npy...'
	np.random.seed(19950420)
	data = np.load(FLAGS.data_dir)
	#data_weights = np.load(FLAGS.weights_dir)
	srd = np.load(FLAGS.srd_dir)
	train_idx = np.load(FLAGS.train_idx)
	valid_idx = np.load(FLAGS.valid_idx)
	labels = get_data.get_label(data, train_idx)
	print "detection rate", np.mean(labels)
	one_epoch_iter = len(train_idx)/FLAGS.batch_size

	print 'reading completed'

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)
	#tf.set_random_seed(19950420)
	print 'showing the parameters...\n'

	#parameterList = FLAGS.__dict__['__flags'].items()
	#parameterList = sorted(parameterList)

	for (key, value) in FLAGS.__dict__['__flags'].items():
		print "%s\t%s"%(key, value)
	print "\n"


	print 'building network...'

	
	hg = model.MODEL(is_training=True)

	global_step = tf.Variable(0, name='global_step', trainable=False)

	learning_rate = tf.train.exponential_decay(FLAGS.learning_rate, global_step, (1.0/FLAGS.lr_decay_times)*(FLAGS.max_epoch*one_epoch_iter), FLAGS.lr_decay_ratio, staircase=True)

	tf.summary.scalar('learning_rate', learning_rate)

	optimizer = tf.train.AdamOptimizer(learning_rate)
	#AdamOptimizer(learning_rate)
	#MomentumOptimizer(learning_rate, 0.9)
	#AdamOptimizer(learning_rate)

	update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
	with tf.control_dependencies(update_ops):
		train_op = optimizer.minimize(hg.total_loss, global_step = global_step)

	merged_summary = tf.summary.merge_all() # gather all summary nodes together
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir,sess.graph)

	sess.run(tf.global_variables_initializer())
	saver = tf.train.Saver(max_to_keep=FLAGS.max_keep)
	
	#saver.restore(sess,FLAGS.checkpoint_path)



	print 'building finished'

	best_loss = 1e10
	best_iter = 0
	smooth_nll_loss=0.0
	smooth_l2_loss=0.0
	smooth_total_loss=0.0
	temp_label=[]	
	temp_indiv_prob=[]

	train_idx_all = np.copy(train_idx)
	"""sum_weights = 0
	for i in train_idx:
		sum_weights += data_weights[i]

	data_weights = data_weights / sum_weights * len(train_idx)"""
	#idx_weights = []
	#for i in train_idx:
	#	idx_weights.append(data_weights[i])

	#count_n_sampled = np.zeros(len(idx_weights))

	for one_epoch in range(FLAGS.max_epoch):
		
		print('epoch '+str(one_epoch+1)+' starts!')

		#train_idx, res= get_random_samples(idx_weights, train_idx_all)
		#count_n_sampled += res
		np.random.shuffle(train_idx)
		np.random.shuffle(valid_idx)
		N_i = int(len(train_idx)/float(FLAGS.batch_size))
		N_j = int(len(valid_idx)/float(FLAGS.batch_size))

		for i in range(N_i):
			j = i % N_j

			start_i = i*FLAGS.batch_size
			end_i = (i+1)*FLAGS.batch_size

			start_j = j*FLAGS.batch_size
			end_j = (j+1)*FLAGS.batch_size

			input_nlcd = get_data.get_nlcd(data, train_idx[start_i:end_i])

			input_valid_nlcd = get_data.get_nlcd(data, valid_idx[start_j:end_j])

			#input_features = np.concatenate((input_nlcd, input_valid_nlcd), 0)
			input_label = np.concatenate(( np.zeros((input_nlcd.shape[0],1))+1, np.zeros((input_nlcd.shape[0],1)) ), 0)
			#

			indiv_prob, nll_loss, l2_loss, total_loss = train_step(sess, hg, merged_summary, summary_writer, input_nlcd, input_valid_nlcd, train_op, global_step)
			
			smooth_nll_loss+=nll_loss
			smooth_l2_loss+=l2_loss
			smooth_total_loss+=total_loss
			
			temp_label.append(input_label)
			temp_indiv_prob.append(indiv_prob)

			current_step = sess.run(global_step) #get the value of global_step

			if current_step%FLAGS.check_freq==0:
				nll_loss = smooth_nll_loss / float(FLAGS.check_freq)
				l2_loss = smooth_l2_loss / float(FLAGS.check_freq)
				total_loss = smooth_total_loss / float(FLAGS.check_freq)
				
				temp_indiv_prob = np.reshape(np.array(temp_indiv_prob),(-1))
				temp_label = np.reshape(np.array(temp_label),(-1))
				ap = average_precision_score(temp_label,temp_indiv_prob)

				temp_indiv_prob = np.reshape(temp_indiv_prob,(-1,FLAGS.r_dim))
				temp_label = np.reshape(temp_label,(-1,FLAGS.r_dim))

				try:
					auc = roc_auc_score(temp_label,temp_indiv_prob)

				except ValueError:
					print 'ytrue error for auc'

				else:
					time_str = datetime.datetime.now().isoformat()
					print "%s\tstep=%d\tauc=%.6f\tap=%.6f\tnll_loss=%.6f\tl2_loss=%.6f\ttotal_loss=%.6f" % (time_str, current_step, auc, ap, nll_loss, l2_loss, total_loss)
					summary_writer.add_summary(MakeSummary('train/auc',auc),current_step)
					summary_writer.add_summary(MakeSummary('train/ap',ap),current_step)

				if (-auc < best_loss):
					best_loss = -auc
					best_iter = current_step
					print 'saving model'
					saved_model_path = saver.save(sess,FLAGS.model_dir+'model',global_step=current_step)

					print 'have saved model to ', saved_model_path
					print "rewriting the number of model to config.py"

					configFile=open(FLAGS.config_dir, "r")
					content=[line.strip("\n") for line in configFile]
					configFile.close()

					for i in range(len(content)):
						if ("checkpoint_path" in content[i]):
							content[i]="tf.app.flags.DEFINE_string('checkpoint_path', './model/model-%d','The path to a checkpoint from which to fine-tune.')"%best_iter
					
					configFile=open(FLAGS.config_dir, "w")
					for line in content:
						configFile.write(line+"\n")
					configFile.close()


				temp_indiv_prob=[]
				temp_label=[]

				smooth_nll_loss = 0
				smooth_l2_loss = 0
				smooth_total_loss = 0

	print 'training completed !'
	

if __name__=='__main__':
	tf.app.run()
