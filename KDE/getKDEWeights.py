# -*- coding:UTF-8 -*-
from pyheatmap.heatmap import HeatMap
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
import math
import plotly.plotly as py
import plotly.graph_objs as go
from sklearn.neighbors.kde import KernelDensity



data = np.load("loc_100sp_nlcd2006_other_img_BCR1314NY_m5_m5.npy")
print data.shape

train_idx = np.load("train_uniform_img_idx.npy")
#valid_idx = np.load("valid_uniform_img_idx.npy")
valid_idx = np.load("valid_uniform_img_idx.npy")
#data = sorted(list(data),cmp)
X = data[:,102:118]

train_X = []
for i in train_idx:
	train_X.append(data[i][102:118])

valid_X = []
for i in valid_idx:
	valid_X.append(data[i][102:118])

kde_train = KernelDensity(kernel='gaussian', bandwidth=0.2).fit(train_X)
print "finish KDE for training"

kde_valid = KernelDensity(kernel='gaussian', bandwidth=0.2).fit(valid_X)
print "finish KDE for validation"

print "fit P"
logP=kde_train.score_samples(X)

print "fit Q"
logQ=kde_valid.score_samples(X)

data_weights = np.zeros(data.shape[0])

for i in range(data.shape[0]):
	data_weights[i]=np.exp(logQ[i]-logP[i])

w = np.ones_like(data_weights)/float(len(data_weights))

n, bines, patches = plt.hist(data_weights, 100, weights=w, alpha=0.75)

plt.show()
np.save("data_weights_KDE", data_weights)


