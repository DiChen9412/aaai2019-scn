import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import model
import get_data
import config 
from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

FLAGS = tf.app.flags.FLAGS

def MakeSummary(name, value):
	"""Creates a tf.Summary proto with the given name and value."""
	summary = tf.Summary()
	val = summary.value.add()
	val.tag = str(name)
	val.simple_value = float(value)
	return summary

#def train_step(input_nlcd, input_label, smooth_ce_loss, smooth_l2_loss,smooth_total_loss):
def train_step(sess, hg, merged_summary, summary_writer, input_nlcd, input_valid_nlcd, train_op, global_step):

	feed_dict={}
	feed_dict[hg.input_nlcd] = input_nlcd
	feed_dict[hg.input_valid_nlcd] = input_valid_nlcd

	temp, step, nll_loss, l2_loss, total_loss, summary= \
	sess.run([train_op, global_step, hg.nll_loss, hg.l2_loss, hg.total_loss,\
	 merged_summary], feed_dict)

	time_str = datetime.datetime.now().isoformat()
	summary_writer.add_summary(summary,step)
	#print input_nlcd[0]
	#print F1, F2
	return nll_loss, l2_loss, total_loss
def main(_):

	print 'reading npy...'
	np.random.seed(19950420)
	data = np.load(FLAGS.data_dir)
	
	srd = np.load(FLAGS.srd_dir)
	train_idx = np.load(FLAGS.train_idx)
	#print train_idx.shape[0]
	valid_idx = np.load(FLAGS.valid_idx)

	labels = get_data.get_label(data, train_idx)
	print "detection rate", np.mean(labels)
	one_epoch_iter = len(train_idx)/FLAGS.batch_size

	print 'reading completed'

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)
	#tf.set_random_seed(19950420)
	print 'showing the parameters...\n'

	#parameterList = FLAGS.__dict__['__flags'].items()
	#parameterList = sorted(parameterList)

	for (key, value) in FLAGS.__dict__['__flags'].items():
		print "%s\t%s"%(key, value)
	print "\n"


	print 'building network...'

	
	hg = model.MODEL(is_training=True)

	global_step = tf.Variable(0, name='global_step', trainable=False)

	learning_rate = tf.train.exponential_decay(FLAGS.learning_rate, global_step, (1.0/FLAGS.lr_decay_times)*(FLAGS.max_epoch*one_epoch_iter), FLAGS.lr_decay_ratio, staircase=True)

	tf.summary.scalar('learning_rate', learning_rate)

	optimizer = tf.train.AdamOptimizer(learning_rate)
	#MomentumOptimizer(learning_rate, 0.9)
	#AdamOptimizer(learning_rate)

	update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
	with tf.control_dependencies(update_ops):
		train_op = optimizer.minimize(hg.total_loss, global_step = global_step)

	merged_summary = tf.summary.merge_all() # gather all summary nodes together
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir,sess.graph)

	sess.run(tf.global_variables_initializer())
	saver = tf.train.Saver(max_to_keep=FLAGS.max_keep)

	print 'building finished'

	best_loss = 1e10
	best_iter = 0
	smooth_nll_loss=0.0
	smooth_l2_loss=0.0
	smooth_total_loss=0.0
	temp_label=[]	
	temp_indiv_prob=[]

	for one_epoch in range(FLAGS.max_epoch):
		
		print('epoch '+str(one_epoch+1)+' starts!')

		input_nlcd = get_data.get_nlcd(data, train_idx)
		input_valid_nlcd = get_data.get_nlcd(data, valid_idx)

		nll_loss, l2_loss, total_loss = train_step(sess, hg, merged_summary, summary_writer, input_nlcd, input_valid_nlcd, train_op, global_step)
			
		
		
		current_step = sess.run(global_step) #get the value of global_step
		
		
		print "step=%d\tnll_loss=%.6f\tl2_loss=%.6f\ttotal_loss=%.6f" % (current_step, nll_loss, l2_loss, total_loss)

		temp_indiv_prob=[]
		temp_label=[]

		smooth_nll_loss = 0
		smooth_l2_loss = 0
		smooth_total_loss = 0



	feed_dict={}
	feed_dict[hg.input_nlcd] = data[:,102:118]

	weights = sess.run([hg.weights_train], feed_dict)[0]
	np.save("data_weights_KLIEP", weights)

	print 'training completed !'
	print 'the best loss on validation is '+str(best_loss)
	print 'the best checkpoint is '+str(best_iter)
	

if __name__=='__main__':
	tf.app.run()
