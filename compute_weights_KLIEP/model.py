import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
FLAGS = tf.app.flags.FLAGS

class MODEL:

	def __init__(self,is_training):
		tf.set_random_seed(19950420)
		r_dim = FLAGS.r_dim
		self.input_nlcd = tf.placeholder(dtype=tf.float32,shape=[None, FLAGS.nlcd_dim],name='input_nlcd')
		self.input_valid_nlcd = tf.placeholder(dtype=tf.float32,shape=[None, FLAGS.nlcd_dim],name='input_valid_nlcd')
		weights_regularizer=slim.l2_regularizer(FLAGS.weight_decay)

		x = slim.fully_connected(self.input_valid_nlcd, 512, weights_regularizer=weights_regularizer, scope='extractor/fc_1', reuse=False)
		x = slim.fully_connected(x, 512, weights_regularizer=weights_regularizer, scope='extractor/fc_2', reuse=False)
		self.logits_valid = slim.fully_connected(x, 1, activation_fn=None, weights_regularizer=weights_regularizer,scope='D/softmax', reuse=False) 

		x = slim.fully_connected(self.input_nlcd, 512, weights_regularizer=weights_regularizer, scope='extractor/fc_1', reuse=True)
		x = slim.fully_connected(x, 512, weights_regularizer=weights_regularizer, scope='extractor/fc_2', reuse=True)
		self.logits_train = slim.fully_connected(x, 1, activation_fn=None, weights_regularizer=weights_regularizer,scope='D/softmax', reuse=True) 


		self.weights_valid = tf.exp(self.logits_valid)
		self.weights_train = tf.exp(self.logits_train)

		self.norm_loss = tf.square(tf.reduce_mean(self.weights_train) -1)

		self.nll_loss = -tf.reduce_mean(tf.log(self.weights_valid))

	
		#	tf.reduce_mean(T*(tf.square(tf.maximum(0.0,upper - self.indiv_prob))) + 
		#		alpha*(1-T)*(tf.square(tf.maximum(0.0, self.indiv_prob-lower))), axis = 1)
		#	* self.input_weights 
		#	)/tf.reduce_sum(self.input_weights)
		#tf.reduce_mean(tf.reduce_sum(tf.nn.sigmoid_cross_entropy_with_logits(labels=self.input_label,logits=self.logits),1))
		
		
		###### loss ##############
		tf.summary.scalar('nll_loss',self.nll_loss)
		tf.summary.scalar('norm_loss',self.norm_loss)

		

		self.l2_loss = tf.add_n(tf.losses.get_regularization_losses())
		self.total_loss = self.l2_loss + self.nll_loss + self.norm_loss *100
		tf.summary.scalar('l2_loss',self.l2_loss)
		tf.summary.scalar('total_loss',self.total_loss)

		
