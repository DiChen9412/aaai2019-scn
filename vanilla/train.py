import tensorflow as tf
import tensorflow.contrib.slim as slim
import numpy as np
import datetime
import model
import get_data
import config 
import os
import sys 
if (os.path.dirname(sys.argv[0])!=''):
	os.chdir(os.path.dirname(sys.argv[0]))

from sklearn.metrics import average_precision_score
from sklearn.metrics import roc_auc_score

FLAGS = tf.app.flags.FLAGS

def get_random_samples(idx_weights, train_idx_all):
	res = np.random.multinomial(len(idx_weights),idx_weights)
	train_idx = []

	for i in range(res.shape[0]):
		for j in range(int(res[i])):
			train_idx.append(train_idx_all[i])

	np.random.shuffle(train_idx)
	return train_idx, res

	
def MakeSummary(name, value):
	"""Creates a tf.Summary proto with the given name and value."""
	summary = tf.Summary()
	val = summary.value.add()
	val.tag = str(name)
	val.simple_value = float(value)
	return summary

#def train_step(input_nlcd, input_label, smooth_ce_loss, smooth_l2_loss,smooth_total_loss):
def train_step(sess, hg, merged_summary, summary_writer, input_label, input_nlcd, input_valid_nlcd, train_op, train_op_D, global_step, alpha):

	feed_dict={}
	feed_dict[hg.input_nlcd] = input_nlcd
	feed_dict[hg.input_valid_nlcd] = input_valid_nlcd
	feed_dict[hg.input_label] = input_label
	feed_dict[hg.keep_prob] = 0.8
	feed_dict[hg.alpha] = alpha

	#step-1
	#nll_loss_D, indiv_prob_D= sess.run([hg.nll_loss_D, hg.indiv_prob_D], feed_dict)

	weights = sess.run(hg.weights, feed_dict)
	feed_dict[hg.input_weights] = weights
	#step-2
	temp, step, nll_loss, l2_loss, total_loss, summary, indiv_prob = \
	sess.run([train_op, global_step, hg.nll_loss, hg.l2_loss, hg.total_loss,\
	 merged_summary, hg.indiv_prob], feed_dict)

	time_str = datetime.datetime.now().isoformat()
	summary_writer.add_summary(summary,step)

	return indiv_prob, nll_loss, l2_loss, total_loss #, indiv_prob_D

def validation_step(sess, hg, data, merged_summary, summary_writer, valid_idx, global_step):

	print 'Validating...'

	all_nll_loss = 0
	all_l2_loss = 0
	all_total_loss = 0

	all_indiv_prob = []
	all_label = []

	real_batch_size=min(FLAGS.batch_size, len(valid_idx))
	for i in range(int( (len(valid_idx)-1)/real_batch_size )+1):

		start = real_batch_size*i
		end = min(real_batch_size*(i+1), len(valid_idx))

		input_nlcd = get_data.get_nlcd(data,valid_idx[start:end])
		input_label = get_data.get_label(data,valid_idx[start:end])
		#input_weights = np.zeros(input_nlcd.shape[0])+1

		feed_dict={}
		feed_dict[hg.input_nlcd]=input_nlcd
		feed_dict[hg.input_label]=input_label
		feed_dict[hg.input_weights] = np.zeros((input_nlcd.shape[0],1))+1
		feed_dict[hg.keep_prob]=1.0
		feed_dict[hg.alpha] = 0

		nll_loss, l2_loss, total_loss, indiv_prob = sess.run([hg.nll_loss, hg.l2_loss, hg.total_loss, hg.indiv_prob],feed_dict)
	
		all_nll_loss += nll_loss*(end-start)
		all_l2_loss += l2_loss*(end-start)
		all_total_loss += total_loss*(end-start)
	
		for i in indiv_prob:
			all_indiv_prob.append(i)
		for i in input_label:
			all_label.append(i)

	all_indiv_prob = np.array(all_indiv_prob)
	all_label = np.array(all_label)

	auc = roc_auc_score(all_label,all_indiv_prob)

	nll_loss = all_nll_loss/len(valid_idx)
	l2_loss = all_l2_loss/len(valid_idx)
	total_loss = all_total_loss/len(valid_idx)

	all_indiv_prob=np.reshape(all_indiv_prob,(-1))
	all_label=np.reshape(all_label,(-1))
	ap = average_precision_score(all_label,all_indiv_prob)

	time_str = datetime.datetime.now().isoformat()

	print "validation results: %s\tauc=%.6f\tap=%.6f\tnll_loss=%.6f\tl2_loss=%.6f\ttotal_loss=%.6f" % (time_str, auc, ap, nll_loss, l2_loss, total_loss)
					
	current_step = sess.run(global_step) #get the value of global_step
	summary_writer.add_summary(MakeSummary('validation/auc',auc),current_step)
	summary_writer.add_summary(MakeSummary('validation/ap',ap),current_step)
	summary_writer.add_summary(MakeSummary('validation/nll_loss',nll_loss),current_step)

	return nll_loss

def main(_):

	print 'reading npy...'
	#np.random.seed(19950420)
	data = np.load( FLAGS.data_dir)
	#data_weights = np.load( FLAGS.weights_dir)
	srd = np.load( FLAGS.srd_dir)
	train_idx = np.load( FLAGS.train_idx)
	valid_idx = np.load( FLAGS.valid_idx)

	labels = get_data.get_label(data, train_idx)
	print "detection rate", np.mean(labels)
	one_epoch_iter = len(train_idx)/FLAGS.batch_size

	print 'reading completed'

	session_config = tf.ConfigProto()
	session_config.gpu_options.allow_growth = True
	sess = tf.Session(config=session_config)
	#tf.set_random_seed(19950420)
	print 'showing the parameters...\n'

	#parameterList = FLAGS.__dict__['__flags'].items()
	#parameterList = sorted(parameterList)

	#for (key, value) in FLAGS.__dict__['__flags'].items():
	#	print "%s\t%s"%(key, value)
	#print "\n"


	print 'building network...'

	
	hg = model.MODEL(is_training=True)

	global_step = tf.Variable(0, name='global_step', trainable=False)

	learning_rate = tf.train.exponential_decay(FLAGS.learning_rate, global_step, (1.0/FLAGS.lr_decay_times)*(FLAGS.max_epoch*one_epoch_iter), FLAGS.lr_decay_ratio, staircase=True)

	tf.summary.scalar('learning_rate', learning_rate)

	optimizer = tf.train.AdamOptimizer(learning_rate)
	#MomentumOptimizer(learning_rate, 0.9)
	#AdamOptimizer(learning_rate)

	update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)

	var_list_F = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="F")
	var_list_G = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="extractor")
	var_list_D = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="D")

	#print var_list_F
	#print var_list_G
	#var_listD = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope="D")

	with tf.control_dependencies(update_ops):
		train_op = optimizer.minimize(hg.total_loss, global_step = global_step, var_list = var_list_F + var_list_G)
		train_op_D = optimizer.minimize(hg.total_loss_D, var_list = var_list_D + var_list_G)

	merged_summary = tf.summary.merge_all() # gather all summary nodes together
	summary_writer = tf.summary.FileWriter(FLAGS.summary_dir,sess.graph)

	sess.run(tf.global_variables_initializer())
	saver = tf.train.Saver(max_to_keep=FLAGS.max_keep)
	#saver.restore(sess,FLAGS.checkpoint_path)

	print 'building finished'

	best_loss = 1e10
	best_iter = 0
	smooth_nll_loss=0.0
	smooth_l2_loss=0.0
	smooth_total_loss=0.0
	temp_label=[]	
	temp_indiv_prob=[]
	temp_indiv_prob_D = []
	temp_label_D = []

	train_idx_all = np.copy(train_idx)
	#idx_weights = []
	#for i in train_idx:
	#	idx_weights.append(data_weights[i])

	#count_n_sampled = np.zeros(len(idx_weights))
	cnt_j = 0
	lim_i = int(len(train_idx)/float(FLAGS.batch_size))
	lim_j = int(len(valid_idx)/float(FLAGS.batch_size))
	for one_epoch in range(FLAGS.max_epoch):
		
		print('epoch '+str(one_epoch+1)+' starts!')

		#train_idx, res= get_random_samples(idx_weights, train_idx_all)
		#count_n_sampled += res
		np.random.shuffle(train_idx)
		np.random.shuffle(valid_idx)
		for i in range(lim_i):
			
			j = cnt_j % lim_j
			
			start_i = i*FLAGS.batch_size
			end_i = (i+1)*FLAGS.batch_size

			start_j = j*FLAGS.batch_size
			end_j = (j+1)*FLAGS.batch_size

			cnt_j += 1


			input_nlcd = get_data.get_nlcd(data,train_idx[start_i:end_i])

			input_valid_nlcd = get_data.get_nlcd(data,valid_idx[start_j:end_j])

			input_label = get_data.get_label(data,train_idx[start_i:end_i])

			#if (one_epoch < FLAGS.pretrain_epoch):
			#	input_weights = np.zeros(input_nlcd.shape[0])+1
			#else:
			#	input_weights = get_data.get_weights(data_weights, train_idx[start:end]) 
			#
			#if (one_epoch < 0.5*FLAGS.max_epoch):
			#	alpha = 0
			#else:
			alpha = 0 # min(1.0, one_epoch/100.0)

			indiv_prob, nll_loss, l2_loss, total_loss = train_step(sess, hg, merged_summary, summary_writer, input_label, input_nlcd, input_valid_nlcd, train_op, train_op_D, global_step, alpha)
			
			smooth_nll_loss+=nll_loss
			smooth_l2_loss+=l2_loss
			smooth_total_loss+=total_loss
			
			temp_label.append(input_label)
			temp_indiv_prob.append(indiv_prob)

			label_D = np.concatenate((np.zeros((input_label.shape[0],1))+1,np.zeros((input_label.shape[0],1))),0)
			
			#temp_label_D.append(label_D)
			#temp_indiv_prob_D.append(indiv_prob_D)

			current_step = sess.run(global_step) #get the value of global_step
			#print current_step
			if current_step%FLAGS.check_freq==0:
				nll_loss = smooth_nll_loss / float(FLAGS.check_freq)
				l2_loss = smooth_l2_loss / float(FLAGS.check_freq)
				total_loss = smooth_total_loss / float(FLAGS.check_freq)
				
				temp_indiv_prob = np.reshape(np.array(temp_indiv_prob),(-1))
				temp_label = np.reshape(np.array(temp_label),(-1))
				ap = average_precision_score(temp_label,temp_indiv_prob)

				temp_indiv_prob = np.reshape(temp_indiv_prob,(-1,FLAGS.r_dim))
				temp_label = np.reshape(temp_label,(-1,FLAGS.r_dim))

				#temp_indiv_prob_D = np.reshape(np.array(temp_indiv_prob_D),(-1))
				#temp_label_D = np.reshape(np.array(temp_label_D),(-1))

				try:
					auc = roc_auc_score(temp_label,temp_indiv_prob)
					#auc_D = roc_auc_score(temp_label_D,temp_indiv_prob_D)

				except ValueError:
					print 'ytrue error for auc'

				else:
					time_str = datetime.datetime.now().isoformat()
					print "%s\tstep=%d\tauc=%.6f\tap=%.6f\tnll_loss=%.6f\tl2_loss=%.6f\ttotal_loss=%.6f" % (time_str, current_step, auc, ap, nll_loss, l2_loss, total_loss)
					
					#summary_writer.add_summary(MakeSummary('D/auc',auc_D),current_step)
					summary_writer.add_summary(MakeSummary('F/auc',auc),current_step)

				temp_indiv_prob=[]
				temp_label=[]

				temp_indiv_prob_D = []
				temp_label_D = []

				smooth_nll_loss = 0
				smooth_l2_loss = 0
				smooth_total_loss = 0

			if current_step%int(one_epoch_iter*FLAGS.save_epoch)==0:
				current_loss = validation_step(sess, hg, data, merged_summary, summary_writer, valid_idx, global_step)
				if current_loss<best_loss:
					print 'current loss:%.10f  which is better than the previous best one!!!'%current_loss
					best_loss = current_loss
					best_iter = current_step

					print 'saving model'
					saved_model_path = saver.save(sess,FLAGS.model_dir+'model',global_step=current_step)

					print 'have saved model to ', saved_model_path
					print "rewriting the number of model to config.py"

					configFile=open( FLAGS.config_dir, "r")
					content=[line.strip("\n") for line in configFile]
					configFile.close()

					for i in range(len(content)):
						if ("checkpoint_path" in content[i]):
							content[i]="tf.app.flags.DEFINE_string('checkpoint_path', './model/model-%d','The path to a checkpoint from which to fine-tune.')"%best_iter
					
					configFile=open( FLAGS.config_dir, "w")
					for line in content:
						configFile.write(line+"\n")
					configFile.close()

	print 'training completed !'
	print 'the best loss on validation is '+str(best_loss)
	print 'the best checkpoint is '+str(best_iter)
	

if __name__=='__main__':
	tf.app.run()
